package com.zhuawa.mycat.domain;

import lombok.Data;

/**
 * 用户信息
 *
 * @author zhibai
 */
@Data
public class User {
    private Long id;
    private String name;
}
