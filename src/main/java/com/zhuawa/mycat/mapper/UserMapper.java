package com.zhuawa.mycat.mapper;

import com.zhuawa.mycat.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户mapper
 *
 * @author zhibai
 */
@Mapper
@Repository
public interface UserMapper {

    @Insert("insert into user(id,name) value (#{id},#{name})")
    int insert(User user);

    @Select("select * from user")
    List<User> selectAll();
}
